package aes

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"
	"testing"
)

func TestAes(t *testing.T) {
	// 正常
	iv := []byte("12345678qwertyui")
	key := []byte("12345678abcdefgh09876543alnkdjfh")
	plainText := []byte("Hi,I'm lady_killer9afzegvrzevgCDSef")
	cipherText, _ := AesEncrypt(plainText, iv, key)
	fmt.Printf("加密后:%s\n", cipherText)
	decryptText, _ := AesDecrypt(cipherText, iv, key)
	fmt.Printf("解密后:%s\n", decryptText)

	// 测试iv大小错误
	iv = []byte("lady_killer9")
	cipherText, err := AesEncrypt(plainText, iv, key)
	if err != nil {
		fmt.Println("测试iv大小错误", err)
		//os.Exit(0)
	}
	// 测试key大小错误
	iv = []byte("12345678qwertyui")
	key = []byte("12345678abcdefgh09876543abc")
	cipherText, err = AesEncrypt(plainText, iv, key)
	if err != nil {
		fmt.Println("测试key大小错误", err)
		//os.Exit(0)
	}
}

func TestOtherAes(t *testing.T) {
	origData := []byte("root")        // 待加密的数据
	key := []byte("ABCDEFGHIJKLMNOP") // 加密的密钥
	log.Println("原文：", string(origData))

	log.Println("------------------ CBC模式 --------------------")
	encrypted := AesEncryptCBC(origData, key)
	log.Println("密文(hex)：", hex.EncodeToString(encrypted))
	log.Println("密文(base64)：", base64.StdEncoding.EncodeToString(encrypted))
	decodeString, err := base64.StdEncoding.DecodeString(base64.StdEncoding.EncodeToString(encrypted))
	if err != nil {
		fmt.Println(err)
	}
	decrypted := AesDecryptCBC(decodeString, key)
	log.Println("解密结果：", string(decrypted))
	//log.Println("------------------ ECB模式 --------------------")
	encrypted = AesEncryptECB(origData, key)
	log.Println("密文(hex)：", hex.EncodeToString(encrypted))
	log.Println("密文(base64)：", base64.StdEncoding.EncodeToString(encrypted))
	decrypted = AesDecryptECB(encrypted, key)
	log.Println("解密结果：", string(decrypted))
	//log.Println("------------------ CFB模式 --------------------")
	encrypted = AesEncryptCFB(origData, key)
	log.Println("密文(hex)：", hex.EncodeToString(encrypted))
	log.Println("密文(base64)：", base64.StdEncoding.EncodeToString(encrypted))
	decrypted = AesDecryptCFB(encrypted, key)
	log.Println("解密结果：", string(decrypted))
}
