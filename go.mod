module jihulab.com/rickyngu/gocrypto

go 1.16

require (
	github.com/ethereum/go-ethereum v1.10.4
	github.com/mr-tron/base58 v1.2.0
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
)
