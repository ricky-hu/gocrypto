package hash

import (
	"crypto"
	"encoding/base64"
	"fmt"
	"github.com/mr-tron/base58"
	"testing"
)

func TestPathFileHash(t *testing.T) {
	hash, err := PathFileHash("../ecc")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(base58.Encode(hash))
	fmt.Println(base64.StdEncoding.EncodeToString(hash))
}

func TestHASH(t *testing.T) {
	hash := HASH("llskdflsdjfhslf", 5, true)
	s := crypto.Hash.String(5)
	fmt.Println(s)
	fmt.Println(hash)
}
