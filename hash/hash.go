package hash

import (
	"crypto"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"golang.org/x/crypto/md4"
	"golang.org/x/crypto/ripemd160"
	"hash"
	"io"
	"log"
	"os"
)

// Hash 哈希值计算
// 需要计算hash明文
// 返回 hash值
//func Hash(b []byte) []byte {
//	// 创建 SHA-256 哈希函数实例
//	hash := sha256.New()
//	// 将数据添加到哈希函数
//	hash.Write(b)
//	// 计算哈希值并获取结果
//	hashValue := hash.Sum(nil)
//	return hashValue
//}

// Hash 文件哈希值计算
// 需要计算hash文件目录
// 返回 hash值 错误
func FileHash(p string) ([]byte, error) {
	file, err := os.Open(p)
	defer file.Close()
	if err != nil {
		fmt.Errorf("读取文件失败！")
		return nil, err
	}
	hash := sha256.New()
	if _, err := io.Copy(hash, file); err != nil {
		log.Fatal(err)
		return nil, err
	}
	sum := hash.Sum(nil)
	return sum, nil
}

// 参数为16进制字符串（必须偶数个,两位16进制为1个字节）
func MD4HexString(text string) string {
	var hashInstance hash.Hash       // 定义哈希实例
	hashInstance = md4.New()         // 实例赋值 "golang.org/x/crypto/md4"
	arr, _ := hex.DecodeString(text) // 十六进制字符串转为十六进制字节数组
	hashInstance.Write(arr)          // 写入哈希实例对象
	bytes := hashInstance.Sum(nil)   // 哈希值追加到参数后面，只获取原始值，不用追加，用nil，返回哈希值字节数组
	return fmt.Sprintf("%x", bytes)  // 格式化输出哈希值
}

// 参数为字符串
func MD4String(text string) string {
	var hashInstance hash.Hash      // 定义哈希实例
	hashInstance = md4.New()        // 实例赋值 "golang.org/x/crypto/md4"
	arr := []byte(text)             // 十六进制字符串转为十六进制字节数组
	hashInstance.Write(arr)         // 写入哈希实例对象
	bytes := hashInstance.Sum(nil)  // 哈希值追加到参数后面，只获取原始值，不用追加，用nil，返回哈希值字节数组
	return fmt.Sprintf("%x", bytes) // 格式化输出哈希值
}

// 根据不同哈希类型进行哈希：md4、md5、sha1、ripemd160、sha256、sha512
func Hash(text string, hashType string, isHex bool) string {
	var hashInstance hash.Hash // 定义哈希实例
	switch hashType {          // 选择哈希类型
	case "md4":
		hashInstance = md4.New() // "golang.org/x/crypto/md4"
	case "md5":
		hashInstance = md5.New()
	case "sha1":
		hashInstance = sha1.New()
	case "ripemd160":
		hashInstance = ripemd160.New() // "golang.org/x/crypto/ripemd160"
	case "sha256":
		hashInstance = sha256.New()
	case "sha512":
		hashInstance = sha512.New()
	}
	if isHex {
		arr, _ := hex.DecodeString(text) // 十六进制字符串转为十六进制字节数组
		hashInstance.Write(arr)          // 写入哈希实例对象
	} else {
		hashInstance.Write([]byte(text)) // 将字符串转换为字节数组，写入哈希对象
	}
	bytes := hashInstance.Sum(nil)  // 哈希值追加到参数后面，只获取原始值，不用追加，用nil，返回哈希值字节数组
	return fmt.Sprintf("%x", bytes) // 格式化输出哈希值
}

func HASH(text string, myhash crypto.Hash, isHex bool) string {
	var hashInstance hash.Hash // 定义哈希实例
	hashInstance = myhash.New()
	if isHex {
		arr, _ := hex.DecodeString(text) // 十六进制字符串转为十六进制字节数组
		hashInstance.Write(arr)          // 写入哈希实例对象
	} else {
		hashInstance.Write([]byte(text)) // 将字符串转换为字节数组，写入哈希对象
	}
	bytes := hashInstance.Sum(nil)  // 哈希值追加到参数后面，只获取原始值，不用追加，用nil，返回哈希值字节数组
	return fmt.Sprintf("%x", bytes) // 格式化输出哈希值
}

// 两次哈希256后的字节数组，第二次是将第一次哈希后的16进制进行哈希
func SHA256Double(text string, isHex bool) []byte {
	hashInstance := sha256.New() // 实例赋值
	if isHex {
		arr, _ := hex.DecodeString(text) // 十六进制字符串转为十六进制字节数组
		hashInstance.Write(arr)          // 写入哈希实例对象
	} else {
		hashInstance.Write([]byte(text)) // 将字符串转换为字节数组，写入哈希对象
	}
	bytes := hashInstance.Sum(nil) // 哈希值追加到参数后面，只获取原始值，不用追加，用nil，返回哈希值字节数组
	hashInstance.Reset()           // 重置哈希实例
	hashInstance.Write(bytes)      // 将第一次哈希值写入哈希对象
	bytes = hashInstance.Sum(nil)  // 获取第二次哈希字节数组
	return bytes
}

// 两次哈希256后的哈希字符串，第二次是将第一次哈希后的16进制进行哈希
func SHA256DoubleString(text string, isHex bool) string {
	return fmt.Sprintf("%x", SHA256Double(text, isHex))
}
