package hash

import (
	"crypto/sha256"
	"hash"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

type Program struct {
	Path      string `json:"path"` //进程所在路径，系统从该路径拉起程序
	Cmd       string `json:"cmd"`  //进程启动脚本
	Signature string `json:"sign"` //签名
	Hash      string `json:"-"`    //path下所有文件名，内容排序计算出的sha256
}

func PathFileHash(path string) ([]byte, error) {
	p := Program{Path: path}
	sha256Digest := sha256.New()
	err := p.list(p.Path, sha256Digest)
	if err != nil {
		return nil, err
	}
	res := sha256Digest.Sum(nil)
	//p.Hash = base58.Encode(res)
	return res, nil
}

func (p *Program) list(dir string, digest hash.Hash) error {
	ls, err := os.ReadDir(dir)
	if err != nil {
		return err
	}
	res := &PathsOrder{Paths: ls}
	sort.Sort(res)
	for _, path := range ls {
		digest.Write([]byte(path.Name()))
		if path.IsDir() {
			err = p.list(filepath.Join(dir, path.Name()), digest)
			if err != nil {
				return err
			}
		}
		err = p.updateHash(filepath.Join(dir, path.Name()), digest)
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *Program) updateHash(file string, digest hash.Hash) error {
	dstFile, err := os.Open(file)
	if err != nil {
		return err
	}
	defer dstFile.Close()
	buf := make([]byte, 1024*64)
	for {
		n, err := dstFile.Read(buf)
		if err != nil {
			if err != io.EOF {
				return err
			}
		}
		if n > 0 {
			digest.Write(buf[0:n])
		}
		if err != nil && err == io.EOF {
			break
		}
	}
	return nil
}

type PathsOrder struct {
	Paths []fs.DirEntry
}

func (ns *PathsOrder) Len() int {
	return len(ns.Paths)
}

func (ns *PathsOrder) Swap(i, j int) {
	ns.Paths[i], ns.Paths[j] = ns.Paths[j], ns.Paths[i]
}

func (ns *PathsOrder) Less(i, j int) bool {
	ii := strings.Compare(ns.Paths[i].Name(), ns.Paths[j].Name())
	return ii >= 0
}
